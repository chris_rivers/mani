//
//  ChorusFramework.h
//  ChorusFramework
//
//  Created by Chris Rivers on 12/09/2016.
//  Copyright © 2016 Chris Rivers. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ChorusFramework.
FOUNDATION_EXPORT double ChorusFrameworkVersionNumber;

//! Project version string for ChorusFramework.
FOUNDATION_EXPORT const unsigned char ChorusFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ChorusFramework/PublicHeader.h>
#import <ManiFramework/Mani.h>

